# mousemover

Python script that wiggles, jiggles, and moves the mouse cursor around without getting in the way

## Known Issues

### High Priority
0. Only tested on mac & windows, not tested on Linux desktop machines

### Low priority
0. May stop screensavers from engaging
0. May stop your desktop from locking due to mouse inactivity
