#!/usr/bin/env python

"""
Simple tool for moving the mouse around when you can't do it yourself

UNINTENDED SIDE EFFECT: May stop screen savers and automatic desktop locking
"""

import pyautogui
import random
import signal
import threading
import datetime


class CONFIG:
    max_move = 16
    delay_user_idle = 1
    delay_user_active = 60
    msg_starting = "start"
    msg_user_moved = "User moved the cursor from {} to {}.  Will wait {}sec before attempting to move it again"
    msg_we_moved = "moving to {},{}"
    msg_exit = "exit"


def log(msg):
    print "{} {}".format(datetime.datetime.utcnow(), msg)


def main():
    shutdown_event = threading.Event()

    def sigint_handler(signum, frame):
        shutdown_event.set()

    signal.signal(signal.SIGINT, sigint_handler)

    last_moved = last_real = pyautogui.position()
    delay = CONFIG.delay_user_active

    log(CONFIG.msg_starting)

    def bounds(value, low, high):
        if value < low:
            return low
        if value > high:
            return high
        return value

    while not shutdown_event.wait(delay):
        position = pyautogui.position()
        if position != last_moved:
            delay = CONFIG.delay_user_active
            log(CONFIG.msg_user_moved.format(
                last_real,
                position,
                delay))
            last_real = last_moved = position
            continue

        delay = CONFIG.delay_user_idle

        # Get new mouse pos
        move_x = position[0] + random.randint(-2, 2)
        move_y = position[1] + random.randint(-2, 2)

        # bound it relative to the original position
        move_x = bounds(move_x, last_real[0] - CONFIG.max_move, last_real[0] + CONFIG.max_move)
        move_y = bounds(move_y, last_real[1] - CONFIG.max_move, last_real[1] + CONFIG.max_move)

        # now move it
        log(CONFIG.msg_we_moved.format(move_x, move_y))
        pyautogui.moveTo(move_x, move_y)
        last_moved = pyautogui.position()

    # All done
    log(CONFIG.msg_exit)


if __name__ == '__main__':
    main()
